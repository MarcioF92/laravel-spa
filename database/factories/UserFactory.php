<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movement;
use App\MovementType;
use App\ThirdParty;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        //'username' => $faker->userName,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
/*
$factory->afterCreating(User::class, function ($user, $faker){
    $user->accounts()->createMany([
        [
            'name' => 'Cuenta banco Galicia',
            'ammount' => 1000,
            'initial_state' => 1
        ],
        [
            'name' => 'Cuenta banco HSBC',
            'ammount' => 3000,
            'initial_state' => 1
        ],
        [
            'name' => 'Caja chica',
            'ammount' => 700,
            'initial_state' => 1
        ],
    ]);
});*/
