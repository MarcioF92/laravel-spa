<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use App\Movement;
use App\MovementType;
use App\ThirdParty;
use App\User;
use Faker\Generator as Faker;

$factory->define(Movement::class, function (Faker $faker) {
    return [
        'type_id' => MovementType::findBySlug('saving')->id,
        'ammount' => rand(1, 10000),
        'third_party_id' => factory(ThirdParty::class)->create()->id,
        'account_id' => Account::all()->random()->id,
        'movement_id' => null,
        'date' => date('Y-m-d'),
        'initial_state' => 1,
    ];
});
