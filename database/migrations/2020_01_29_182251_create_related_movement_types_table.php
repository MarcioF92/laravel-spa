<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelatedMovementTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('related_movement_types', function (Blueprint $table) {
            $table->unsignedBigInteger('type1_id');
            $table->unsignedBigInteger('type2_id');
        });

        Schema::table('related_movement_types', function (Blueprint $table) {
            $table->foreign('type1_id')->references('id')->on('movement_types');
            $table->foreign('type2_id')->references('id')->on('movement_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('related_movement_types');
    }
}
