<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('user_id');
            $table->double('ammount')->default(0);
            $table->unsignedBigInteger('third_party_id')->nullable();
            $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('movement_id')->nullable();
            $table->boolean('initial_state')->default(0);
            $table->text('comments')->nullable();
            $table->timestamps();
        });

        Schema::table('movements', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('movement_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('third_party_id')->references('id')->on('third_parties');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('movement_id')->references('id')->on('movements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
    }
}
