<?php

use App\MovementType;
use Illuminate\Database\Seeder;

class RelatedMovementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MovementType::findByslug('entries')->relatedTypes()->sync(MovementType::findByslug('credits'));
        MovementType::findByslug('credits')->relatedTypes()->sync(MovementType::findByslug('entries'));

        MovementType::findByslug('expenses')->relatedTypes()->sync(MovementType::findByslug('debits'));
        MovementType::findByslug('debits')->relatedTypes()->sync(MovementType::findByslug('expenses'));
    }
}
