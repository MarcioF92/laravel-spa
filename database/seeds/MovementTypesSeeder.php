<?php

use App\MovementType;
use Illuminate\Database\Seeder;

class MovementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(MovementType::class)->createMany([
            ['name' => 'Saving'],
            ['name' => 'Entries', 'available_on_initial_state' => 0],
            ['name' => 'Expenses', 'available_on_initial_state' => 0],
            ['name' => 'Credits'],
            ['name' => 'Debits'],
        ]);
    }
}
