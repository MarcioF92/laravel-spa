<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;
use Tests\CreatesApplication;
use Tests\TestsHelper;

class FeatureTestCase extends TestCase
{
    use RefreshDatabase, CreatesApplication, TestsHelper;

    public function seeErrors(array $fields)
    {
        foreach ($fields as $name => $errors) {
            foreach ((array) $errors as $message) {
                $this->seeInElement(
                    "#field_{$name}.has-error .help-block", $message
                );
            }
        }
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
    }
}
