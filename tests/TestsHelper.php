<?php

namespace Tests;

use App\Account;
use App\Movement;
use App\MovementType;
use App\ThirdParty;
use App\User;

trait TestsHelper
{
    protected $defaultUser;

    public function defaultUser(array $attributes = [])
    {
        if($this->defaultUser) {
            $this->defaultUser->movements()->delete();
            return $this->defaultUser;
        }

        return $this->defaultUser = factory(User::class)->create($attributes);
    }

    public function setAccounts()
    {
        $accountsData = [
            [
                'name' => 'Cuenta banco Galicia',
                'ammount' => 1000,
                'initial_state' => 1
            ],
            [
                'name' => 'Cuenta banco HSBC',
                'ammount' => 2000,
                'initial_state' => 1
            ],
            [
                'name' => 'Caja chica',
                'ammount' => 750,
                'initial_state' => 1
            ],
        ];
        foreach ($accountsData as $account) {
            Account::create($account);
        }
    }

    public function setUserMovements($user)
    {
        $cliente = ThirdParty::create([
            'name' => 'Cliente 1',
            'user_id' => $user->id
        ]);

        $proveedor = ThirdParty::create([
            'name' => 'Proveedor 1',
            'user_id' => $user->id
        ]);

        factory(Movement::class)->createMany([
            [
                'ammount' => 4000,
                'type_id' => MovementType::findBySlug('credits')->id,
                'third_party_id' => $cliente->id,
                'user_id' => $user->id,
                'date' => '2020-01-01'
            ],
            [
                'ammount' => 300,
                'type_id' => MovementType::findBySlug('debits')->id,
                'third_party_id' => $proveedor->id,
                'user_id' => $user->id,
                'date' => '2020-01-05'
            ],
            [
                'ammount' => 400,
                'type_id' => MovementType::findBySlug('expenses')->id,
                'third_party_id' => $cliente->id,
                'user_id' => $user->id,
                'date' => '2020-01-10'
            ],
            [
                'ammount' => 3000,
                'type_id' => MovementType::findBySlug('entries')->id,
                'third_party_id' => $proveedor->id,
                'user_id' => $user->id,
                'date' => '2020-02-01'
            ]
        ]);

        return $user;
    }

}
