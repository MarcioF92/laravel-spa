<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class ThirdPartyTest extends FeatureTestCase
{
    function test_a_user_can_list_it_third_parties()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->get('/api/third_party');

        $response->assertStatus(200)
            ->assertSee('Cliente 1')
            ->assertSee('Proveedor 1');
    }

    function test_a_user_can_limit_it_third_parties_list()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/third_party', [
            'limit' => 1
        ]);

        $response->assertStatus(200);
        $this->assertTrue($this->getResultCount($response->getContent()) == 1);
    }

    private function getResultCount($result)
    {
        return count(json_decode($result)->thirdParties);
    }
}
