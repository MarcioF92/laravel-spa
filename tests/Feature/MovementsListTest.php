<?php

namespace Tests\Feature;

use App\Account;
use App\Movement;
use App\MovementType;
use App\ThirdParty;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class MovementsListTest extends FeatureTestCase
{
    function test_user_can_filter_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $this->filterByMovementTypeTest();
        $this->filterByDatesTest1();
        $this->filterByDatesTest2();
        $this->filterByAccountTest();
        $this->filterByThirdPartyTest();
        $this->manyFiltersTest();
    }

    private function filterByMovementTypeTest()
    {
        $typeSlug = ['saving'];
        $response = $this->json('get', '/api/movements', [
            'type_slug' => $typeSlug
        ]);

        $this->checkResults($response,
            Movement::whereIn('type_id', [MovementType::findByslug($typeSlug[0])->id])->get(),
            Movement::whereNotIn('type_id', [MovementType::findByslug($typeSlug[0])->id])->get()
        );
    }

    private function filterByDatesTest1()
    {
        $dateFrom = '2020-01-10';
        $response = $this->json('get', '/api/movements', [
            'date_from' => $dateFrom
        ]);

        $this->checkResults($response,
            Movement::where('date', '>=', $dateFrom)->get(),
            Movement::where('date', '<', $dateFrom)->get()
        );
    }

    private function filterByDatesTest2()
    {
        $dateFrom = '2020-01-01';
        $dateTo = '2020-01-10';
        $response = $this->json('get', '/api/movements', [
            'date_from' => $dateFrom,
            'date_to' => $dateTo
        ]);

        $this->checkResults($response,
            Movement::whereBetween('date', [$dateFrom, $dateTo])->get(),
            Movement::whereNotBetween('date', [$dateFrom, $dateTo])->get()
        );
    }

    private function filterByAccountTest()
    {
        $accountId = [Account::where('name', 'Cuenta banco Galicia')->first()->id];
        $response = $this->json('get', '/api/movements', [
            'account_id' => $accountId,
        ]);

        $this->checkResults($response,
            Movement::whereIn('account_id', [$accountId])->get(),
            Movement::whereNotIn('account_id', [$accountId])->get()
        );
    }

    private function filterByThirdPartyTest()
    {
        $thirdPartyId = ThirdParty::where('name', 'Cliente 1')->first()->id;
        $response = $this->json('get', '/api/movements', [
            'third_party_id' => $thirdPartyId,
        ]);

        $this->checkResults($response,
            Movement::where('third_party_id', $thirdPartyId)->get(),
            Movement::whereNot('third_party_id', $thirdPartyId)->get()
        );
    }

    private function manyFiltersTest()
    {
        $filters = [
            'account_id' => [Account::where('name', 'Cuenta banco Galicia')->first()->id],
            'type_slug' => ['saving', 'credits'],
            'date_from' => '2020-01-01'
        ];
        $response = $this->json('get', '/api/movements', $filters);

        $mayAppear = Movement::whereIn('account_id', [Account::where('name', 'Cuenta banco Galicia')->first()->id])
            ->whereIn('type_id', [MovementType::findByslug('saving')->id, MovementType::findByslug('credits')->id])
            ->where('date', '>=', '2020-01-01')
            ->get();

        $mayNotAppear = Movement::whereNotIn('account_id', [Account::where('name', 'Cuenta banco Galicia')->first()->id])
                            ->whereNotIn('type_id', [MovementType::findByslug('saving')->id, MovementType::findByslug('credits')->id])
                            ->where('date', '<', '2020-01-01')
                            ->get();

        $this->checkResults($response,
            $mayAppear,
            $mayNotAppear
        );
    }

    function test_user_can_order_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/movements', [
            'order_by' => 'date',
            'order' => 'DESC'
        ]);

        $this->checkResults($response, Movement::orderBy('date', 'DESC')->get());
    }

    function test_user_can_limit_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/movements', [
            'limit' => 3,
        ]);

        $this->checkResults($response, Movement::limit(3)->get());
    }

    function test_user_can_limit_movements_with_offset()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/movements', [
            'limit' => 3,
            'offset' => 2
        ]);

        $this->checkResults($response, Movement::offset(2)->limit(3)->get());
    }

    /*
     * Checking results
     */
    private function checkResults($response, $mayAppear, $mayNotAppear = [])
    {
        $response->assertStatus(200);
        $this->checkAppearing(
            $response->getContent(),
            $mayAppear
        );

        if(count($mayNotAppear))
            $this->checkAppearing(
                $response->getContent(),
                $mayNotAppear,
                false
            );
    }

    private function checkAppearing($response, $movements, $mayAppear = true)
    {
        $responsedMovementsId = collect(json_decode($response)->movements)->pluck('id')->sortBy(function ($id) { return $id; });
        $comparedMovementsId = collect($movements)->pluck('id')->sortBy(function ($id) { return $id; });
        $function = $mayAppear ? 'assertTrue' : 'assertFalse';
        $this->$function(array_values($responsedMovementsId->toArray()) == array_values($comparedMovementsId->toArray()));
    }
}
