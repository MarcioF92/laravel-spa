<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class MovementTypesTest extends FeatureTestCase
{
    function test_user_can_list_movement_types()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/movement_types');

        $response->assertStatus(200)
            ->assertSee('Saving')
            ->assertSee('Credits')
            ->assertSee('Debits')
            ->assertSee('Entries')
            ->assertSee('Expenses');
    }
}
