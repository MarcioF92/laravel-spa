<?php

namespace Tests\Feature;

use App\Account;
use App\Movement;
use App\MovementType;
use App\ThirdParty;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class MovementsTest extends FeatureTestCase
{
    function test_user_can_list_it_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('get', '/api/movements');

        $response->assertStatus(200);

        /*$this->assertTrue(
            $this->validateResponseData($response->getContent())
        );*/
    }

    function test_user_can_create_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movementData = [
            'type_id' => MovementType::findBySlug('credits')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $response = $this->json('post', '/api/movements', $movementData);

        $response->assertStatus(201);

        $this->assertDatabaseHas('movements', $movementData);

        /* Second movement */
        $secondMovementData = [
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => $user->movements()->first()->id,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $response = $this->json('post', '/api/movements', $movementData);

        $response->assertStatus(201);

        $this->assertDatabaseHas('movements', $movementData);

    }

    function test_user_cant_create_movement_with_invalid_data()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movement1 = $user->movements()->create([
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ]);

        $movementData = [
            'type_id' => MovementType::findBySlug('expenses')->id,
            'ammount' => 500,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => $movement1->id,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $response = $this->json('post', '/api/movements', $movementData);

        $response->assertStatus(422)
            ->assertSee('The related movement type is invalid.');

        $this->assertDatabaseMissing('movements', $movementData);
    }

    function test_user_can_edit_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movement1 = $user->movements()->create([
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => 100.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ]);

        $movementData = [
            'type_id' => MovementType::findBySlug('debits')->id,
            'ammount' => 999.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $response = $this->json('put', '/api/movements/'.$movement1->id, $movementData);

        $movementData['id'] = $movement1->id;

        $response->assertStatus(200);

        $this->assertDatabaseHas('movements', $movementData);
    }

    function test_user_cant_edit_movement_with_invalid_data()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movement1 = $user->movements()->create([
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => 100.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ]);

        $movement2 = $user->movements()->create([
            'type_id' => MovementType::findBySlug('expenses')->id,
            'ammount' => 100.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ]);

        $movementData = [
            'type_id' => MovementType::findBySlug('debits')->id,
            'ammount' => 999.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => $movement2->id,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $response = $this->json('put', '/api/movements/'.$movement1->id, $movementData);

        $movementData['id'] = $movement1->id;

        $response->assertStatus(422);

        $this->assertDatabaseMissing('movements', $movementData);
    }

    function test_user_can_delete_movement()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movementData = [
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => 100.0,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement1 = $user->movements()->create($movementData);

        $this->assertDatabaseHas('movements', $movementData);

        $response = $this->json('delete', '/api/movements/'.$movement1->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('movements', [
            'id' => $movement1->id
        ]);
    }

    function test_user_cant_admin_movements_when_not_has_initial_state()
    {
        $user = $this->defaultUser();

        Airlock::actingAs($user);

        $response = $this->json('get', '/api/movements');

        $response->assertRedirect('/initial_state');
    }

    function test_user_can_get_related_movements()
    {

    }

    /*
     * Private functions
     */
    private function setMovements($user)
    {
        $cliente = ThirdParty::create([
            'name' => 'Cliente 1',
            'user_id' => $user->id
        ]);

        $proveedor = ThirdParty::create([
            'name' => 'Proveedor 1',
            'user_id' => $user->id
        ]);

        $movement1 = factory(Movement::class)->create([
            'type_id' => MovementType::findBySlug('credits')->id,
            'ammount' => 1000,
            'third_party_id' => $cliente->id,
            'user_id' => $user->id,
            'initial_state' => 0
        ]);

        return $user;
    }

    private function validateResponseData($data)
    {
        $arrayData = json_decode($data)[0];

        return $arrayData->type_id == MovementType::findBySlug('credits')->id;
    }

}
