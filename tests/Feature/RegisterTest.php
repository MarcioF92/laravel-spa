<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureTestCase;

class RegisterTest extends FeatureTestCase
{
    function test_guest_can_create_user()
    {
        $userData = [
            'name' => 'Marcio Fuentes',
            'email' => 'marcio@fuentes.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $response = $this->json('POST', '/api/register', $userData);

        $response
            ->assertStatus(201)
            ->assertSee('token')
            ->assertSee('user');

        unset($userData['password_confirmation']);
        $this->assertDatabaseHas('users', $userData);
    }

    function test_guest_cant_create_an_existing_user()
    {
        $userData = [
            'email' => 'marcio@fuentes.com',
            'password' => 'password',
        ];
        factory(User::class)->create($userData);

        $userData['password_confirmation'] = 'password';

        $response = $this->json('POST', '/api/register', $userData);

        $response
            ->assertStatus(422)
            ->assertJson(['message' => "The given data was invalid."])
            ->assertSee("The email has already been taken.");
    }

    function test_gest_cant_create_a_user_with_invalid_data()
    {
        $userData = [
            'email' => 'asdasd',
            'password' => '',
            'password_confirmation' => 'password'
        ];

        $response = $this->json('POST', '/api/register', $userData);

        $response
            ->assertStatus(422)
            ->assertJson(['message' => "The given data was invalid."])
            ->assertSee("The email must be a valid email address.")
            ->assertSee("The password field is required.");
    }
}
