<?php

namespace Tests\Feature;

use App\Account;
use App\Movement;
use App\MovementType;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;

class AccountsTest extends FeatureTestCase
{
    function test_a_user_can_list_accounts($accountsNames = [])
    {
        $user = $this->defaultUser();
        Airlock::actingAs(
            $user,
            ['*']
        );
        $this->setAccounts();

        $response = $this->json('GET', '/api/accounts');

        $response->assertStatus(200)
                ->assertSee('Cuenta banco Galicia')
                ->assertSee('Cuenta banco HSBC')
                ->assertSee('Caja chica');

        foreach ($accountsNames as $account) {
            $response->assertSee($account);
        }
    }

    function test_a_user_can_create_account()
    {
        $user = $this->defaultUser();
        Airlock::actingAs(
            $user
        );

        $accountName = 'Nueva cuenta';
        $ammount = 300;
        $response = $this->post('/api/accounts', [
                'name' => $accountName,
                'ammount' => $ammount,
                'initial_state' => 0
            ]);

        $account = Account::where('name', $accountName)->first();

        $this->assertDatabaseHas('accounts', [
            'name' => $accountName,
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('movements', [
            'type_id' => MovementType::findByslug('saving')->id,
            'user_id' => $user->id,
            'ammount' => $ammount,
            'account_id' => $account->id,
            'initial_state' => 0
        ]);

        $response->assertStatus(201)
            ->assertJson([
                'success' => true
            ]);
    }

    function test_a_user_can_edit_account()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $account = $user->accounts()->first();
        $accountName = 'Nuevo nombre';
        $response = $this->patch('/api/accounts/'.$account->id, [
            'name' => $accountName
        ]);

        $this->assertDatabaseHas('accounts', [
            'name' => $accountName,
            'id' => $account->id,
            'user_id' => $user->id
        ]);

        $response->assertStatus(200)
            ->assertSee('Account edited successfully');
    }

    function test_a_user_can_delete_account()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $account = $user->accounts()->first();
        $response = $this->delete('/api/accounts/'.$account->id);

        $this->assertDatabaseMissing('accounts', [
            'id' => $account->id,
            'user_id' => $this->defaultUser()->id
        ]);

        $response->assertStatus(200)
            ->assertSee('Account deleted successfully');
    }

    function test_a_user_can_transfer_money_through_two_accounts()
    {
        $user = $this->defaultUser();
        Airlock::actingAs(
            $user,
            ['*']
        );
        $this->setAccounts();
        $this->setUserMovements($user);

        $account1 = Account::where('name', 'Cuenta banco Galicia')->first();
        $account2 = Account::where('name', 'Cuenta banco HSBC')->first();
        $ammount = 300;

        $response = $this->post('/api/transfer', [
            'from_id' => $account1->id,
            'to_id' => $account2->id,
            'ammount' => $ammount,
            'date' => date('Y-m-d')
        ]);

        $this->assertDatabaseHas('movements', [
                'type_id' => MovementType::findByslug('expenses')->id,
                'ammount' => $ammount,
                'account_id' => $account1->id,
            ])
            ->assertDatabaseHas('movements', [
                'type_id' => MovementType::findByslug('entries')->id,
                'ammount' => $ammount,
                'account_id' => $account2->id,
            ]);
    }

    private function initAccounts()
    {
        $account1 = Account::where('name', 'Cuenta banco Galicia')->first();
        $account2 = Account::where('name', 'Cuenta banco HSBC')->first();

        factory(Movement::class)->create([
            'type_id' => MovementType::findByslug('saving')->id,
            'account_id' => $account1->id,
            'ammount' => 1000
        ]);

        factory(Movement::class)->create([
            'type_id' => MovementType::findByslug('saving')->id,
            'account_id' => $account2->id,
            'ammount' => 1000
        ]);
    }
}
