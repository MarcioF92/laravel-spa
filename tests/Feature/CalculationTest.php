<?php

namespace Tests\Feature;

use App\Account;
use App\MovementType;
use App\ThirdParty;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class CalculationTest extends FeatureTestCase
{
    function test_user_can_see_it_calculations()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $movement1Data = [
            'type_id' => MovementType::findBySlug('credits')->id,
            'ammount' => 500,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement1 = $user->movements()->create($movement1Data);

        $movement2Data = [
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => 500,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => $movement1->id,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement2 = $user->movements()->create($movement2Data);

        $movement3Data = [
            'type_id' => MovementType::findBySlug('debits')->id,
            'ammount' => 1000,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement3 = $user->movements()->create($movement3Data);

        $movement4Data = [
            'type_id' => MovementType::findBySlug('expenses')->id,
            'ammount' => 800,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => $movement3->id,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement4 = $user->movements()->create($movement4Data);

        $movement5Data = [
            'type_id' => MovementType::findBySlug('credits')->id,
            'ammount' => 600,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement5 = $user->movements()->create($movement5Data);

        $movement6Data = [
            'type_id' => MovementType::findBySlug('debits')->id,
            'ammount' => 400,
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 0,
        ];

        $movement6 = $user->movements()->create($movement6Data);

        $response = $this->get('/api/calculations');

        $response->assertStatus(200);
        $this->assertTrue($this->validateCalculations($response->getContent()));
    }

    private function validateCalculations($data)
    {
        $arrayData = json_decode($data);
        return $arrayData->economic == 6050 && $arrayData->financial == 10050;
    }
}
