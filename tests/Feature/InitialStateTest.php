<?php

namespace Tests\Feature;

use App\Account;
use App\MovementType;
use App\ThirdParty;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Airlock\Airlock;
use Tests\FeatureTestCase;
use Tests\TestCase;

class InitialStateTest extends FeatureTestCase
{
    function test_a_user_can_view_it_initial_state()
    {
        $user = $this->defaultUser();

        Airlock::actingAs($user);
        $this->setAccounts();
        $this->setUserMovements($user);

        $response = $this->json('GET', '/api/initial_state');

        $response->assertStatus(200)
            ->assertSee('movements');

        $this->assertDatabaseHas('movements', [
            'user_id' => $user->id,
            'initial_state' => 1
        ]);
    }

    function test_a_user_can_set_an_initial_state_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movementData = [
            'type_id' => MovementType::findBySlug('saving')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 1
        ];

        $response = $this->json('POST', '/api/movements', $movementData);

        $response->assertStatus(201)
            ->assertJson(['success' => true]);

        $movementData['user_id'] = $user->id;

        $this->assertDatabaseHas('movements', $movementData);
    }

    function test_a_user_cant_set_invalid_movment_type_in_an_initial_state_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movementData = [
            'type_id' => MovementType::findBySlug('entries')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 1
        ];

        $response = $this->json('POST', '/api/movements', $movementData);

        $response->assertStatus(422)
            ->assertJson(['success' => false])
            ->assertJson(['message' => "The selected Movement Type is invalid"]);

        $movementData['user_id'] = $user->id;

        $this->assertDatabaseMissing('movements', $movementData);
    }

    function test_a_user_cant_set_invalid_movment_id_in_an_initial_state_movements()
    {
        $user = $this->defaultUser();
        Airlock::actingAs($user);
        $this->setAccounts();

        $movementData = [
            'type_id' => MovementType::findBySlug('saving')->id,
            'ammount' => rand(1, 10000),
            'third_party_id' => factory(ThirdParty::class)->create()->id,
            'account_id' => Account::all()->random()->id,
            'movement_id' => null,
            'date' => date('Y-m-d'),
            'initial_state' => 1
        ];

        $movement = $user->movements()->create($movementData);

        $movementData['movement_id'] = $movement->id;

        $response = $this->json('POST', '/api/movements', $movementData);

        $response->assertStatus(422)
            ->assertJson(['success' => false])
            ->assertJson(['message' => "The Relationed Movement must be null"]);

        $movementData['user_id'] = $user->id;

        $this->assertDatabaseMissing('movements', $movementData);
    }
}
