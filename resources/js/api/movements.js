import axios from 'axios';

const client = axios.create({
    baseURL: '/api',
});

export default {
    all(params) {
        return client.get('movements', {
            params: params
        });
    },
    find(id) {
        return client.get(`movements/${id}`);
    },
    update(id, data) {
        return client.put(`movements/${id}`, data);
    },
    delete(id) {
        return client.delete(`movements/${id}`);
    },
    create(data) {
        return client.post('movements', data);
    },
};
