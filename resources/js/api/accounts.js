import axios from 'axios';

const client = axios.create({
    baseURL: '/api',
});

export default {
    all(params) {
        return client.get('accounts', params);
    },
    find(id) {
        return client.get(`accounts/${id}`);
    },
    update(id, data) {
        return client.put(`accounts/${id}`, data);
    },
    delete(id) {
        return client.delete(`accounts/${id}`);
    },
    create(data) {
        return client.post('accounts', data);
    },
};
