import axios from 'axios';

const client = axios.create({
    baseURL: '/api',
});

export default class Api {
    constructor(base) {
        this.base = base;
    }

    client() {
        return client;
    }
    all(params) {
        return client.get(this.base, {
            params: params
        });
    }
    find(id) {
        return client.get(this.base+`/${id}`);
    }
    update(id, data) {
        return client.put(this.base+`/${id}`, data);
    }
    delete(id) {
        return client.delete(this.base+`/${id}`);
    }
    create(data) {
        return client.post(this.base, data);
    }
};
