/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//import Vue from 'vue'
import VueRouter from 'vue-router'

window.Vue = require('vue');
window.Vue.use(VueRouter);

import VModal from 'vue-js-modal'

Vue.use(VModal, { dialog: true })

/*import App from './views/App'
import Login from './views/auth/Login';*/

Vue.component('auth', require('./views/auth/Auth.vue').default);
Vue.component('app', require('./views/App.vue').default);

const app = new Vue({
    el: '#app',
});
