@extends('layouts.app')

@section('component')
    <form action="{{ url('/logout') }}" method="post">
        @csrf
        <button type="submit">Cerrar sesión</button>
    </form>
    <app></app>
@endsection
