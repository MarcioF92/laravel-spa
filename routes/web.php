<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

Route::middleware('guest')->group(function(){
    Route::get('login', 'SpaController@login')->name('login');
    Route::get('register', 'SpaController@login')->name('register');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
});

Route::middleware('auth')->group(function(){
    Route::middleware('initial_state')->group(function() {
        Route::get('/', 'SpaController@index');
        Route::get('movements', 'SpaController@index');
    });

    Route::get('/{any}', 'SpaController@index')->where('any', '.*');
    Route::post('logout', 'Auth\LoginController@logout');
});



