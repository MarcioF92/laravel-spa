<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

*/

Route::group(['namespace' => 'Api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'auth:airlock'], function () {
        Route::get('/users', 'UsersController@index');
        Route::get('/users/{user}', 'UsersController@show');
        Route::put('/users/{user}', 'UsersController@update');
        Route::delete('/users/{user}', 'UsersController@destroy');

        Route::get('logout', 'AuthController@logout');

        Route::resource('accounts', 'AccountsController');
        Route::get('initial_state', 'MovementsController@initialState');
        Route::apiResource('third_party', 'ThirdPartyController');

        Route::get('movement_types', 'MovementTypesController@index');

        Route::group(['middleware' => 'initial_state'], function () {
            Route::apiResource('movements', 'MovementsController');
            Route::get('calculations', 'CalculationsController@getCalculations');
            Route::post('transfer', 'MovementsController@transfer');
        });
    });
});

