<?php

namespace App;

use App\Repositories\AccountsRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Account extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function toArray()
    {
        $accountsRepository = new AccountsRepository($this);
        $array = parent::toArray();
        $array['ammount'] = $accountsRepository->getAmmount();
        return $array;
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($account){
            self::beforeSaving($account);
        });

        self::created(function($account){
            $temporalData = Session::get('temporal_account_data');
            $movement = Auth::user()->movements()->create([
                'type_id' => MovementType::findByslug('saving')->id,
                'account_id' => $account->id,
                'ammount' => $temporalData->ammount,
                'date' => isset($temporalData->date) ? $temporalData->date : date('Y-m-d'),
                'initial_state' => $temporalData->initial_state
            ]);
            Session::remove('temporal_account_data');
        });

        self::updating(function($account){
            self::beforeSaving($account);
        });

        self::updated(function($account){
            $temporalData = Session::get('temporal_account_data')->toArray();
            unset($temporalData['name']);
            Auth::user()->movements()
                ->where('type_id', MovementType::findByslug('saving')->id)
                ->where('account_id', $account->id)
                ->first()
                ->update($temporalData);
            Session::remove('temporal_account_data');
        });

        self::deleting(function($account){
            Movement::where('type_id', MovementType::findByslug('saving')->id)
                ->where('account_id', $account->id)
                ->delete();
        });


    }

    public static function beforeSaving($account)
    {
        Session::put('temporal_account_data', clone $account);
        unset($account->ammount);
        unset($account->initial_state);
        unset($account->date);
        if(Auth::check())
            $account->user_id = Auth::user()->id;
    }
}
