<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable as SluggableModel;

class Sluggable extends Model
{
    use SluggableModel;

    private $slugSource = 'name';

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => $this->slugSource
            ]
        ];
    }

    public static function findByslug($slug)
    {
        return Self::where('slug', $slug)->first();
    }
}
