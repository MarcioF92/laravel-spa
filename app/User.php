<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Airlock\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function movements()
    {
        return $this->hasMany(Movement::class, 'user_id', 'id');
    }

    public function thirdParties()
    {
        return $this->hasMany(ThirdParty::class);
    }

    public function getInitialState()
    {
        return $this->movements()->where('initial_state', '=', 1)->get();
    }

    public function hasInitialState()
    {
        return $this->movements()->where('initial_state', '=', 1)->count();
    }
}
