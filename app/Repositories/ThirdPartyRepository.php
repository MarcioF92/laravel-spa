<?php


namespace App\Repositories;

use App\ThirdParty;
use Illuminate\Support\Facades\Auth;

class ThirdPartyRepository extends Repository implements RepositoryInterface
{
    // Constructor to bind model to repo
    public function __construct(ThirdParty $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        $query = Auth::user()
            ->thirdParties()
            ->where('name', 'LIKE', '%'.request()->input('name', '').'%')
            ->orderBy(request()->input('order_by', 'created_at'), request()->input('order', 'DESC'));

        if(request()->input('limit', false))
            $query = $query->limit(request()->input('limit'));

        return $query->get();
    }

    public function create(array $data)
    {
        return Auth::user()->thirdParties()->create($data);
    }

}
