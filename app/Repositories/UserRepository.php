<?php


namespace App\Repositories;

use App\User;

class UserRepository extends Repository implements RepositoryInterface
{
    // Constructor to bind model to repo
    public function __construct(User $model)
    {
        $this->model = $model;
    }
}
