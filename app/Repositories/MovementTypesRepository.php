<?php


namespace App\Repositories;

use App\Movement;
use App\MovementType;
use Illuminate\Support\Facades\Auth;

class MovementTypesRepository extends Repository implements RepositoryInterface
{
    public function __construct(MovementType $model)
    {
        $this->model = $model;
    }

    public function getBySlug($slug)
    {
        return $this->model::findByslug($slug);
    }

    public function initialStateMovementTypes()
    {
        return $this->model::where('available_on_initial_state', 1)->pluck('id')->all();
    }

}
