<?php


namespace App\Repositories;

use App\Account;
use App\Movement;
use App\MovementType;
use App\User;
use Illuminate\Support\Facades\Auth;

class AccountsRepository extends Repository implements RepositoryInterface
{
    // Constructor to bind model to repo
    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return Auth::user()->accounts()->get();
    }

    public function update(array $data, $id = false)
    {
        $ammount = isset($data['ammount']) ? $data['ammount'] : false;

        unset($data['ammount']);

        $account = $this->model->update($data);

        if($ammount !== false)
            $movement = Auth::user()->movements()
                ->where('type_id', MovementType::findByslug('saving')->id)
                ->where('account_id', $this->model->id)
                ->first()
                ->update([
                    'ammount' => $ammount,
                ]);

        return $account;
    }

    public function getAmmount()
    {
        return Auth::user()->movements()
                    ->where('account_id', $this->model->id)
                    ->whereIn('type_id', [MovementType::findByslug('saving')->id, MovementType::findByslug('entries')->id])
                    ->sum('ammount')
                -
                Auth::user()->movements()
                    ->where('account_id', $this->model->id)
                    ->where('type_id', [MovementType::findByslug('expenses')->id])
                    ->sum('ammount');
    }

}
