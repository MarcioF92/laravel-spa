<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository implements RepositoryInterface
{
    // model property on class instances
    protected $model;
    public $errors;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->all();
    }

    // create a new record in the database
    public function create(array $data)
    {
        $model = $this->model->create($data);
        return $model;
    }

    // update record in the database
    public function update(array $data, $id = false)
    {
        if(!$id) :
            $record = $this->getModel();
        else :
            $record = $this->show($id);
        endif;

        $record->update($data);
        return true;
    }

    // remove record from the database
    public function delete($id)
    {
        $this->model->findOrFail($id);
        $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function truncate(){
        $this->model->truncate();
    }

    public function first(){
        if($this->model->count())
            return $this->model->first();
        return new $this->model();
    }

    public function where($column, $operator, $value, $one = false){
        $query = $this->model->where($column, $operator, $value);
        if($one)
            return $query->first();
        return $query->get();
    }

    public function pluck($name, $id){
        return $this->model->pluck($name, $id);
    }

    public function ordered($column, $order = 'ASC'){
        return $this->model->orderBy($column, $order)->get();
    }

    /* Errors */
    public function addError($message){
        $this->errors[] = $message;
        return true;
    }

    public function getErrors(){
        return $this->errors;
    }
}
