<?php


namespace App\Repositories;

use App\Account;
use App\Movement;
use App\MovementType;
use App\User;
use Illuminate\Support\Facades\Auth;

class MovementsRepository extends Repository implements RepositoryInterface
{
    // Constructor to bind model to repo
    public function __construct(Movement $model)
    {
        $this->model = $model;
    }

    public function all()
    {

        $dateFrom = request()->input('date_from', '0000-00-00') == '' ? '0000-00-00' : request()->input('date_from', '0000-00-00');
        $dateTo = request()->input('date_to', date('Y-m-d')) == '' ? date('Y-m-d') : request()->input('date_to', date('Y-m-d'));

        $query = Auth::user()->movements()
            ->whereBetween('date', [$dateFrom, $dateTo])
            ->orderBy(request()->input('order_by', 'created_at'), request()->input('order', 'DESC'));

        if(request()->input('type_slug', false))
            $query = $query->whereIn('type_id', MovementType::whereIn('slug', request()->input('type_slug'))->pluck('id')->all());

        if(request()->input('third_party_id', false))
            $query = $query->where('third_party_id', request()->input('third_party_id'));

        if(request()->input('account_id', false))
            $query = $query->whereIn('account_id', request()->input('account_id'));

        $query = $query->orderBy(request()->input('order_by', 'created_at'), request()->input('order', 'DESC'));

        if(request()->input('offset', false))
            $query = $query->offset(request()->input('offset'));

        if(request()->input('limit', false))
            $query = $query->limit(request()->input('limit'));

        return $query->with('type')
                     ->with('account')
                     ->with('thirdParty')
                     ->with('movement')
                     ->get();
    }

    public function initialState()
    {
        return Auth::user()->getInitialState();
    }

    public function create(array $data)
    {
        if(isset($data['type_slug'])) {
            $data['type_id'] = MovementType::findByslug($data['type_slug'])->id;
            unset($data['type_slug']);
        }
        $movement = Auth::user()->movements()->create($data);
        return $this->with('type')
            ->with('account')
            ->with('thirdParty')
            ->with('movement')
            ->find($movement->id);
    }

    public function validateMovementsRelation($typeId, $movementId)
    {
        $movement = $this->model::find($movementId);
        return $movement->type->relatedTypes()->where('id', $typeId)->count();
    }

    public function getCalculations()
    {
        return [
            'economic' => $this->getEconomicCalculation(),
            'financial' => $this->getFinancialCalculation()
        ];
    }

    private function getEconomicCalculation()
    {
        $savings = $this->getSumByType('saving');
        $entries = $this->getSumByType('entries');
        $expenses = $this->getSumByType('expenses');
        return $savings + $entries - $expenses;
    }

    private function getFinancialCalculation()
    {
        $savings = $this->getSumByType('saving');
        $noPayedCredits = $this->getNoPayed('credits');
        $noPayedDebits = $this->getNoPayed('debits');
        $entries = $this->getWithNoMovementAssociated('entries');
        $expenses = $this->getWithNoMovementAssociated('expenses');

        return $savings + $noPayedCredits + $entries - $noPayedDebits - $expenses;

    }

    private function getSumByType($typeSlug)
    {
        return Auth::user()->movements()->where('type_id', MovementType::findByslug($typeSlug)->id)->sum('ammount');
    }

    private function getNoPayed($typeSlug)
    {
        $movements = Auth::user()->movements()->where('type_id', MovementType::findByslug($typeSlug)->id)->get();

        $sum = 0;
        foreach ($movements as $movement) {
            $sum += $movement->ammount - $movement->relations()->sum('ammount');
        }

        return $sum;
    }

    private function getWithNoMovementAssociated($typeSlug)
    {
        return Auth::user()
            ->movements()
            ->where('type_id', MovementType::findByslug($typeSlug)->id)
            ->whereNull('movement_id')
            ->sum('ammount');
    }
}
