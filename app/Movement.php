<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(MovementType::class);
    }

    public function relations()
    {
        return $this->hasMany(Movement::class, 'movement_id');
    }

    public function movement()
    {
        return $this->belongsTo(Movement::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function thirdParty()
    {
        return $this->belongsTo(ThirdParty::class);
    }

    public function getByTypes($types)
    {
        if(is_string($types))
            $types = [$types];

        return $this->whereIn('type_id',
            MovementType::whereIn('slug', $types)->pluck('id')->all()
        )->get();
    }

    public function getTotalByType($typeSlug)
    {
        return $this->where('slug', $typeSlug)->sum('ammount');
    }
}
