<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasInitialState
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->hasInitialState() && $request->input('initial_state') != 1)
            //return response()->json('You need to configure your initial state first', 401);
            return redirect('/initial_state');

        return $next($request);
    }
}
