<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function index($uri = '/')
    {
        return view('spa');
    }

    public function login($uri = '/')
    {
        return view('login');
    }
}
