<?php

namespace App\Http\Controllers\Api;

use App\Account;
use App\Http\Controllers\Controller;
use App\Movement;
use App\MovementType;
use App\Repositories\AccountsRepository;
use App\Repositories\MovementsRepository;
use App\Repositories\MovementTypesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MovementsController extends Controller
{
    /**
     * @var MovementsRepository
     */
    private $model;

    public function __construct(MovementsRepository $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['movements' => $this->model->all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validateIntegrityData($request);

        if(!$validation['success'])
            return response()->json($validation, 422);

        if($movement = $this->model->create($request->all()))
            return response()->json(['success' => true, 'movement' => $movement], 201);

        return response()->json(['success' => false], 422);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movement  $movement
     * @return \Illuminate\Http\Response
     */
    public function show(Movement $movement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movement  $movement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movement $movement)
    {
        $validation = $this->validateIntegrityData($request);

        if(!$validation['success'])
            return response()->json($validation, 422);

        if($movement->update($request->all()))
            return response()->json(['success' => true], 200);

        return response()->json(['success' => false], 422);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movement  $movement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movement $movement)
    {
        $movement->delete();
        return ['success' => true];
    }

    public function initialState()
    {
        return response()->json(['movements' => $this->model->initialState()]);
    }

    public function transfer(Request $request)
    {
        $request->validate([
            'from_id' => 'required|exists:accounts,id',
            'to_id' => 'required|exists:accounts,id',
            'ammount' => 'required|numeric',
            'date' => 'required|date'
        ]);

        $accountsRepository = new AccountsRepository(new Account());

        $this->model->create([
            'type_slug' => 'expenses',
            'account_id' => $request->input('from_id'),
            'ammount' => $request->input('ammount'),
            'date' => $request->input('date'),
            'comments' => 'Transferencia hacia cuenta "'.$accountsRepository->show($request->input('to_id'))->name.'"'
        ]);

        $this->model->create([
            'type_slug' => 'entries',
            'account_id' => $request->input('to_id'),
            'ammount' => $request->input('ammount'),
            'date' => $request->input('date'),
            'comments' => 'Transferencia desde cuenta "'.$accountsRepository->show($request->input('from_id'))->name.'"'
        ]);

        return response()->json(['success' => true], 200);
    }

    /*
     * Validations
     */
    private function validateIntegrityData($request)
    {
        $request->validate([
            'ammount' => 'required|numeric',
            'third_party_id' => 'sometimes|exists:third_parties,id',
            'account_id' => 'sometimes|exists:accounts,id',
            'movement_id' => 'nullable|exists:movements,id',
            'date' => 'date',
            'initial_state' => 'boolean'
        ]);

        if($request->input('type_id', false)) {
            $request->validate([
                'type_id' => 'exists:movement_types,id'
            ]);
        } else {
            $request->validate([
                'type_slug' => 'exists:movement_types,slug'
            ]);
        }

        if($request->input('initial_state'))
            return $this->validateInitialState($request);

        if(!is_null($request->input('movement_id', null)))
            return $this->validateRelationedMovement($request);

        return ['success' => true];

    }

    private function validateInitialState($request)
    {
        $movementTypesRepository = new MovementTypesRepository(new MovementType());

        if(!in_array($request->input('type_id', optional($movementTypesRepository->getBySlug($request->input('type_slug', '')))->id), $movementTypesRepository->initialStateMovementTypes()))
            return [
                'success' => false,
                'message' => 'The selected Movement Type is invalid'
            ];

        if(!is_null($request->input('movement_id')))
            return [
                'success' => false,
                'message' => 'The Relationed Movement must be null'
            ];

        return ['success' => true];
    }

    private function validateRelationedMovement($request)
    {
        if(!$this->model->validateMovementsRelation(
            $request->input('type_id'),
            $request->input('movement_id')
        ))
            return ['success' => false, 'message' => 'The related movement type is invalid.'];
    }
}
