<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\MovementsRepository;
use Illuminate\Http\Request;

class CalculationsController extends Controller
{
    /**
     * @var MovementsRepository
     */
    private $model;

    public function __construct(MovementsRepository $model)
    {
        $this->model = $model;
    }
    public function getCalculations()
    {
        return $this->model->getCalculations();
    }
}
