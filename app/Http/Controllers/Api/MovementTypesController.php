<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\MovementTypesRepository;
use Illuminate\Http\Request;

class MovementTypesController extends Controller
{
    /**
     * @var MovementTypesRepository
     */
    private $model;

    public function __construct(MovementTypesRepository $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['movement_types' => $this->model->all()], 200);
    }
}
