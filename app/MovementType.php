<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovementType extends Sluggable
{
    protected $guarded = ['id'];

    public function relatedTypes()
    {
        return $this->belongsToMany(MovementType::class, 'related_movement_types', 'type1_id', 'type2_id');
    }
}
